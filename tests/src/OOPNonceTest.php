<?php
/**
 * Created by PhpStorm.
 * User: mkowal
 * Date: 23.05.2019
 * Time: 16:42
 */

use Extensions\Nonce\OOPNonce;
use Extensions\Nonce\Exception\OOPNonceException;

/**
 * Class OOPNonceTest
 */
class OOPNonceTest extends \PHPUnit\Framework\TestCase
{

    /**
     * Test action
     * @var string
     */
    private $testAction = 'test_action';

    /**
     * Test name
     * @var string
     */
    private $testName = 'test_name';

    /**
     * Example url for test purpose
     * @var string
     */
    private $testUrl = 'http://www.example.url';

    /**
     * default rootDirectory of wp. Plugin should be installed in "wp-content/plugins/"
     * @var string
     */
    private $wpRootDir = __DIR__.'/../../../../../';

    /**
     * Setting of root directory of wordPress, for include of libraries used there.
     */
    protected function setUp()
    {
        include($this->wpRootDir . 'wp-load.php');
        parent::setUp();
    }

    /**
     * Test method for OOPNonceCreate
     */
    public function testOOPNonceCreate()
    {
        $nonceFromWp = wp_create_nonce($this->testAction);
        $nonceObject = new OOPNonce($this->testAction);
        $nonceObject->OOPNonceCreate();
        $nonceNumber = $nonceObject->nonce();

        $this->assertSame($nonceFromWp, $nonceNumber);
    }

    /**
     * Test method for OOPNonceField
     */
    public function testOOPNonceField()
    {
        $nonceFromWp = wp_nonce_field($this->testAction, $this->testName);
        $nonceObject = new OOPNonce($this->testAction, $this->testName);
        $nonceField = $nonceObject->OOPNonceField();

        $this->assertSame($nonceFromWp, $nonceField);
    }

    /**
     * Test method for OOPNonceUrl
     */
    public function testOOPNonceUrl()
    {
        $nonceFromWp = wp_nonce_url($this->testUrl, $this->testAction, $this->testName);
        $nonceObject = new OOPNonce($this->testAction, $this->testName);
        $nonceUrl = $nonceObject->OOPNonceUrl($this->testUrl);

        $this->assertSame($nonceFromWp, $nonceUrl);
    }

    /**
     * Test method for OOPNonceVerify
     *
     * @throws OOPNonceException
     */
    public function testOOPNonceVerify()
    {
        $nonceFromWp = wp_create_nonce($this->testAction);
        $nonceFromWpVerify = wp_verify_nonce($nonceFromWp, $this->testAction);
        $nonceObject = new OOPNonce($this->testAction, $this->testName);
        $nonce = $nonceObject->OOPNonceCreate();
        $nonceVerify = $nonceObject->OOPNonceVerify($nonce);

        switch ($nonceFromWpVerify){
            case 1:
                $nonceReturn = 'the nonce has been generated in the past 12 hours or less.';
                break;
            case 2:
                $nonceReturn = 'the nonce was generated between 12 and 24 hours ago.';
                break;
            default:
                throw new OOPNonceException('Nonce is invalid or its life time has been ended');
        }

        $this->assertSame($nonceVerify, $nonceReturn);
    }

    /**
     * Test method for checkActionUrl
     */
    public function testCheckActionUrl()
    {
        $method = self::getMethod('checkActionUrl');
        $nonceObject = new OOPNonce($this->testAction, $this->testName);

        $this->assertTrue($method->invoke($nonceObject, $this->testUrl));

    }

    /**
     * Helper method for Reflecting non public methods
     * @param $name
     * @return ReflectionMethod
     */
    protected static function getMethod($name)
    {
        $method = new ReflectionMethod('Extensions\Nonce\OOPNonce', $name);
        $method->setAccessible(true);
        return $method;
    }

}
