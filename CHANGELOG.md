**1.0.1** - Changing version number and providing Changelog

**1.0.0** - Providing description and improving directory of wordPress

**0.1.1** - Creating a OOPNonce field and verification, adding tests and condiguring 

**0.1.0** - OOPNonce create and url implementation

**0.0.5** - Adding own Exceptions, with possibility to manipulate message length

**0.0.4** - Implementing Abstract class with all know parameters logic

**0.0.3** - Adding all parameters used in `wp_nonce_*()` to interface for 
further implementation

**0.0.2** - Removing of unwanted files from repository

**0.0.1** - First implementation of composer
