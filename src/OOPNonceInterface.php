<?php

namespace Extensions\Nonce;

/**
 * Interface OOPNonceInterface
 *
 * @package Extensions\Nonce
 */
interface OOPNonceInterface
{

    /**
     * @param $nonce
     */
    public function defineNonce(string $nonce);

    /**
     * @return string
     */
    public function nonce(): string;

    /**
     * @param $action
     */
    public function defineAction(string $action);

    /**
     * @return string
     */
    public function action(): string;

    /**
     * @param string $name
     */
    public function defineName(string $name);

    /**
     * @return string
     */
    public function name(): string;

    /**
     * @param string $actionUrl
     */
    public function defineActionUrl(string $actionUrl);

    /**
     * @return string
     */
    public function actionUrl(): string;

    /**
     * @param bool $referrer
     */
    public function defineReferrer(bool $referrer);

    /**
     * @return bool
     */
    public function referrer(): bool;

    /**
     * @param bool $echo
     */
    public function defineEcho(bool $echo);

    /**
     * @return bool
     */
    public function echo() : bool;

}