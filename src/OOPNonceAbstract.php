<?php
/**
 * Created by PhpStorm.
 * User: mkowal
 * Date: 22.05.2019
 * Time: 00:54
 */

namespace Extensions\Nonce;

/**
 * Class OOPNonceAbstract
 *
 * @package Extensions\Nonce
 */
abstract class OOPNonceAbstract implements OOPNonceInterface
{
    /**
     * @var string
     */
    private $nonce;

    /**
     * @var string
     */
    private $action;

    /**
     * @var string
     */
    private $name;

    /**
     * @var string
     */
    private $actionUrl;

    /**
     * @var bool
     */
    private $referrer;

    /**
     * @var bool
     */
    private $echo;

    /**
     *
     * include all dependencies files so it could be run anywhere
     * OOPNonceAbstract constructor.
     *
     * @param string $action
     * @param string $name
     */
    public function __construct(string $action, string $name, string $wpRootDir)
    {
        include($wpRootDir.'wp-load.php');

        $this->defineAction($action);
        $this->defineName($name);
    }

    /**
     * @param string $nonce
     * @return OOPNonceInterface
     */
    public function defineNonce(string $nonce) : OOPNonceInterface
    {
        $this->nonce = $nonce;
        return $this;
    }

    /**
     * @return string
     */
    public function nonce(): string
    {
        return $this->nonce;
    }

    /**
     * @param string $action
     * @return OOPNonceInterface
     */
    public function defineAction(string $action) : OOPNonceInterface
    {
        $this->action = $action;
        return $this;
    }

    /**
     * @return string
     */
    public function action(): string
    {
        return $this->action;
    }

    /**
     * @param string $name
     * @return OOPNonceInterface
     */
    public function defineName(string $name) : OOPNonceInterface
    {
        $this->name = $name;
        return $this;
    }

    /**
     * @return string
     */
    public function name(): string
    {
        return $this->name;
    }

    /**
     * @param string $actionUrl
     * @return OOPNonceInterface
     */
    public function defineActionUrl(string $actionUrl) : OOPNonceInterface
    {
        $this->actionUrl = $actionUrl;
        return $this;
    }

    /**
     * @return string
     */
    public function actionUrl(): string
    {
        return $this->actionUrl;
    }

    /**
     * @param bool $referrer
     * @return OOPNonceInterface
     */
    public function defineReferrer(bool $referrer) : OOPNonceInterface
    {
        $this->referrer = $referrer;
        return $this;
    }

    /**
     * @return bool
     */
    public function referrer(): bool
    {
        return $this->referrer;
    }

    /**
     * @param bool $echo
     * @return OOPNonceInterface
     */
    public function defineEcho(bool $echo) : OOPNonceInterface
    {
        $this->echo = $echo;
        return $this;
    }

    /**
     * @return bool
     */
    public function echo (): bool
    {
        return $this->echo;
    }

}