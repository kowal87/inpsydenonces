<?php

/**
 * Created by PhpStorm.
 * User: mkowal
 * Date: 22.05.2019
 * Time: 13:50
 */
namespace Extensions\Nonce\Exception;

/**
 * Class OOPNonceException
 *
 * @package Extensions\Nonce\Exception
 */
class OOPNonceException extends \Exception
{

    const MESSAGE_TRIM_SIZE_DEFAULT = 250;

    /**
     * Optional length of exception message.
     * @var
     */
    protected static $messageTrimSize = 0;

    /**
     * Full, uncut, complete message used for detailed debugging
     *
     * @var
     */
    protected $completeMessage;

    /**
     * OOPNonceException constructor.
     *
     * @param string $message
     * @param int $code
     * @param \Exception|null $previous
     */
    public function __construct(string $message = "", int $code = 0, \Exception $previous = null)
    {
        $this->defineCompleteMessage($message);

        $trimSize = static::messageTrimSize();
        if ($trimSize > 0) {
            if (strlen($message) > $trimSize) {
                $message = substr($message, 0, $trimSize);
                //we want to show at least one letter
                if ($trimSize > 3) {
                    $message = substr($message, 0, $trimSize - 3) . "...";
                }
            }
        }
        parent::__construct($message, $code, $previous);
    }

    /**
     * Returns the value of $messageTrimSize or default
     * static::MESSAGE_TRIM_SIZE_DEFAULT if not set (null).
     * @return int
     */
    public static function messageTrimSize() : int
    {
        if (is_int(static::$messageTrimSize)) {
            return static::$messageTrimSize;
        }
        return static::MESSAGE_TRIM_SIZE_DEFAULT;
    }

    /**
     * Configuring other value of message, different then default
     *
     * @param int $size
     * @return int
     */
    public static function enableMessageTrimming(int $size = 0) : int
    {
        if (!is_int($size) || $size < 0) {
            throw new \InvalidArgumentException(
                '$size must be a positive integer, to set own values');
        }
        return static::$messageTrimSize = $size;
    }

    /**
     * Disables message trimming.
     *
     * Sets the value of $messageTrimSize which explicitly disables message
     * trimming.
     */
    public static function disableMessageTrimming()
    {
        static::$messageTrimSize = 0;
    }

    /**
     * Gets the $completeMessage which stores the complete, uncut message for
     * detailed debugging in contrast to the obtainable message via
     * `getMessage()` method which is trimmed by default to deliver a quick,
     * clear and not over obtrusive information.
     *
     * @return string
     */
    public function completeMessage() : string
    {
        return $this->completeMessage;
    }

    /**
     * Sets the $completeMessage which is meant to store the complete, uncut
     * message for potential full debugging in contrast to the content
     * obtainable via `getMessage()` which is trimmed by default for clear,
     * quick and not over obtrusive debug.
     *
     * @param string $completeMessage
     * @return OOPNonceException
     */
    protected function defineCompleteMessage(string $completeMessage) : self
    {
        $this->completeMessage = (string) $completeMessage;

        return $this;
    }

}