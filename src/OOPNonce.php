<?php
/**
 * Created by PhpStorm.
 * User: mkowal
 * Date: 22.05.2019
 * Time: 13:28
 */

namespace Extensions\Nonce;

use Extensions\Nonce\Exception\OOPNonceException;

/**
 * Class OOPNonce
 *
 * @package Extensions\Nonce
 */
class OOPNonce extends OOPNonceAbstract
{
    /**
     * OOPNonce constructor.
     *
     * Parameters defaults, are set from WP documentation of wp_nonce_*()
     * default rootDirectory of wp. Plugin should be installed in "wp-content/plugins/"
     *
     * @param string $action
     * @param string $name
     * @param string $wpRootDir
     */
    public function __construct(string $action = '-1', string $name = '_wpnonce',
        string $wpRootDir = __DIR__.'/../../../../') {
        parent::__construct($action, $name, $wpRootDir);

    }

    /**
     * Creates a nonce string
     * @return string
     */
    public function OOPNonceCreate() : string
    {
        $nonce = wp_create_nonce($this->action());
        $this->defineNonce($nonce);

        return $nonce;
    }

    /**
     * Creating a nonce for field tags
     * @param bool $referrer
     * @param bool $echo
     * @return string
     */
    public function OOPNonceField(bool $referrer = true, bool $echo = true) : string
    {
        $this->OOPNonceCreate();
        $this->defineReferrer($referrer);
        $this->defineEcho($echo);

        return wp_nonce_field($this->action(), $this->name(), $this->referrer(), $this->echo());
    }

    /**
     * Verifying a nonce
     * @return string
     * @throws OOPNonceException
     */
    public function OOPNonceVerify(string $nonce = '') : string
    {
        if (empty($nonce) && isset($_REQUEST[$this->name()])){
            $nonce = $_REQUEST[$this->name()];
        }

        $nonceVerify = wp_verify_nonce($nonce, $this->action());

        switch ($nonceVerify){
            case 1:
                $result = 'the nonce has been generated in the past 12 hours or less.';
                break;
            case 2:
                $result = 'the nonce was generated between 12 and 24 hours ago.';
                break;
            default:
                $result = $nonceVerify;
                throw new OOPNonceException('Nonce is invalid or its life time has been ended');
        }
        return $result;
    }

    /**
     * Creates an url with nonce
     *
     * @param string $actionUrl
     * @return string
     * @throws OOPNonceException
     */
    public function OOPNonceUrl(string $actionUrl) : string
    {
        if (!$this->checkActionUrl($actionUrl)){
            throw new OOPNonceException(
                'URL is not valid, you have used '. $actionUrl. '. Please verify your data!');
        }

        return wp_nonce_url($this->actionUrl(), $this->action(), $this->name());
    }

    /**
     * Method to check if actionUrl is valid and if it can be used in OOPNonce
     * @param string $actionUrl
     * @return bool
     */
    protected function checkActionUrl(string $actionUrl) : bool
    {
        if (filter_var($actionUrl, FILTER_VALIDATE_URL)){
            $this->defineActionUrl($actionUrl);
            return true;
        }
        return false;
    }
}