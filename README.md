# Object Oriented Nonces for WordPress

This plugin has been written in OOP way as original nonces functions are not 
working is such methodology. Thanks to such solution we are not using hardcoded
keys and action, giving us possibility of customization, and ability to extend
methods on implementations.

## Requiments

- PHP 7.0
- Composer
- Wordpress 5.0.3
- PHPUnit 7

## Installation
Please install plugin in plugin directory of WordPress default __wp-content/plugins/__
However it is possible to install it at any directory, but it will be needed to 
point such directory on Object creation.

After putting plugin in directory please use 
```sh
$ composer install
```

## Testing

Executes commands below to run tests:

```sh
$ composer install
$ vendor/bin/phpunit
```

## Usage

### Initialize object of OOPNonce:

**```$nonceObject = new OOPNonce('action', 'name')```**

It is possible to do not add any parameters with creation of OOPNonce, so it will be 
taken from default value

**```$nonceObject = new OOPNonce()```**

Also if plugin will be installed in other directory then default __wp-content/plugins/__
it is possible to point new directory of installation so plugin will include all 
dependencies from WordPress, but it will be need also to add all other parameters

**```$nonceObject = new OOPNonce('action', 'name', 'new/plugin/directory')```**

### Creating a nonce:
Next to create the nonce method OOPNonceCreate() need to be used on __$nonceObject__

**```$nonceObject->OOPNonceCreate()```**

### Creating a nonce field:
To create nonce filed out of object please use:

**```$nonceObject->OOPNonceField()```**

It is possible to add parameters of __referrer__ and __echo__ to method call as bool.
Both parameters by default are set to __true__

**```$nonceObject->OOPNonceField(true, true)```**

### Creating a nonce url:
To create a nonce url from object please use:

**```$nonceObject->OOPNonceUrl('www.example.url')```**

It is mandatory to add an url to call

## Verifying a nonce

To check a nonce from __$_REQUEST__ please use:

**```$nonceObject->OOPNonceVerify()```**

If you want to verify already existing nonce please add it as parametere

**```$nonceObject->OOPNonceVerify($nonce)```**

## Notes
It is an simple solution to handle nonces in object oriented programming. I have added
here also own __Exceptions__ method that allow to modify length of message for simpler
debugging. Additionally verification of nonces works in the same time for already created
nonces or for ones that came from GET/POST requests. 
It is possible to run that code form any directory, as it is loading dependencies from 
main WordPress.
